FROM lachlanevenson/k8s-kubectl:v1.9.6

# Install envsubst
RUN apk add -U gettext

ADD k8s/ /tmp/
ADD scripts/ /usr/local/bin/

ENTRYPOINT []
