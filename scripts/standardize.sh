#!/bin/sh

if [ $CI_JOB_STAGE == "pre-prod" ] || [ $CI_JOB_STAGE == "prod" ]
then
    export K8S_ENV_SLUG=$CI_JOB_STAGE
else
    ### sh doesn't let you redirect strings, e.g. <<<, so we'll create a file first
    echo $CI_COMMIT_REF_SLUG > /tmp/temp
    IFS='-' read first second < /tmp/temp
    export K8S_ENV_SLUG=branch-$first
fi