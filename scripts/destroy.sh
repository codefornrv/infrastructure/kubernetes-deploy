#!/bin/sh

setup-kubeconfig.sh

# Standardize environment variables that might be too long
. standardize.sh

kubectl delete ingress,service,deployment,Certificate -l "app=$K8S_ENV_SLUG" -n "$CI_JOB_STAGE"