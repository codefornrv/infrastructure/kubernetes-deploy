#!/bin/sh

# Setup kubeconfig
mkdir /root/.kube
envsubst < /tmp/kubeconfig > /root/.kube/config