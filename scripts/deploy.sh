#!/bin/sh

setup-kubeconfig.sh

# Standardize environment variables that might be too long
. standardize.sh

# Substitute env variables into our templates
mkdir /tmp/deploy
envsubst < /tmp/cert.yml > /tmp/deploy/cert.yml
envsubst < /tmp/deployment.yml > /tmp/deploy/deployment.yml
envsubst < /tmp/service.yml > /tmp/deploy/service.yml
envsubst < /tmp/ingress.yml > /tmp/deploy/ingress.yml
envsubst < /tmp/ingress-redirect.yml > /tmp/ingress-redirect-template.yml

# Check for REDIRECT_DOMAINS and create redirect ingress
if [ "$REDIRECT_DOMAINS" != "" ]
then
    IFS=","
    for domain in $REDIRECT_DOMAINS
    do
        cat<<EOT >>/tmp/ingress-redirect-template.yml
  - host: "$domain"
    http:
      paths:
      - backend:
          serviceName: default-http-backend
          servicePort: 80
        path: /
EOT
    done
    cp /tmp/ingress-redirect-template.yml /tmp/deploy/
fi

# Deploy to Kubernetes
kubectl apply -f /tmp/deploy/
kubectl rollout status -n "$CI_JOB_STAGE" -w "deployment/$K8S_ENV_SLUG"